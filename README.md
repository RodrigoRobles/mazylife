# MazyLife

A retro maze game written in pure javascript.

Get the yellow key and run to the door while avoiding the monsters and using the tools to your advantage. There are various types of monsters, scenery elements, and tools that can either hinder or assist you along the way.

![screenshot](https://drive.google.com/uc?id=1Y7YStwthaSodis7U15Nf2fykzG2yfjHH&export=download)

This is a retro maze game inspired by games from Atari, Odyssey2, Apple 2, and MSX. The mazes are pseudo-random procedurally generated.

You can download the Android app of the game here: https://play.google.com/store/apps/details?id=com.robles8bit.mazylife

## Features

* Retro game graphics and sound effects;
* Infinite pseudo-random generated stages;
* Four types of monsters, one of which can steal tools;
* Scenery elements: key doors, intermittent doors, teleporters;
* Magnet tool;
* Scoreboard;
* Joystick support.

## Architecture

* The game is written in pure JavaScript, without using any frameworks or even jQuery;
* The animation runs on a canvas at 60 FPS;
* The resolution of 272x176 was chosen to emulate a "landscape" 80's video game.